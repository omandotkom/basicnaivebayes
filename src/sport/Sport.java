/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sport;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author omandotkom
 * NIM : 16102126
 */
public class Sport {

    static List<Sentence> listKalimat = new ArrayList<Sentence>();

    public static double getKata(String kata, String TAG) {
        //mencari jumlah kata tertentu berdasarkan TAG nya
        //misal kata a pada TAG SPORT ada berapa, dan pada NOT SPORT ada berapa
        double result = 0;
        for (int i = 0; i < listKalimat.size(); i++) {
            if (listKalimat.get(i).getTAG().equals(TAG)) {
                String[] katakata = listKalimat.get(i).getKalimat().split(" ");
                for (int u = 0; u < katakata.length; u++) {
                    if (katakata[u].equalsIgnoreCase(kata)) {
                        result++;
                    }

                }
            }
        }
        return result;
    }

    public static int totalWord(String TAG) {
        //Menghitung total kata berdasarkan TAG
        int result = 0;
        for (int i = 0; i < listKalimat.size(); i++) {
            if (listKalimat.get(i).getTAG().equalsIgnoreCase(TAG)) {
                result = result + listKalimat.get(i).getKalimat().split(" ").length;
            }
        }
        return result;
    }

    public static double getProbablebyWords(String word, String TAG) {
        //Prosedur untuk menghitung probabilitas
        String[] possibleSelector = {"a", "great", "game", "the", "election", "was", "over", "very", "clean", "match", "but", "forgottable", "it", "close"};
       //possible selector itu yang dapetin 14
        double hasil = (getKata(word, TAG) + 1) / (totalWord(TAG) + possibleSelector.length);
        return hasil;
    }

    public static void main(String[] args) {

        //TRAINING DATA
        listKalimat.add(new Sentence("A great game", "SPORT"));
        listKalimat.add(new Sentence("The election was over", "NOT SPORT"));
        listKalimat.add(new Sentence("Very clean match", "SPORT"));
        listKalimat.add(new Sentence("A clean but forgottable game", "SPORT"));
        listKalimat.add(new Sentence("It was a close election", "NOT SPORT"));

        double sportValue = 1000;
        double notsportValue = 1000;
        final String LABEL1 = "SPORT";
        final String LABEL2 = "NOT SPORT";
        String[] kata = "a very close game".split(" ");
        for (String kata1 : kata) {
            double probSport = getProbablebyWords(kata1, LABEL1);
            double probNotSport = getProbablebyWords(kata1, LABEL2);
            System.out.println("P(" + kata1 + "| " + LABEL1 + " )= " + probSport);
            sportValue = sportValue * probSport;
            System.out.println("P(" + kata1 + "| " + LABEL2 + " )= " + probNotSport);
            notsportValue = notsportValue * probNotSport;
        }

        System.out.println("\n\nSPORT VALUE " + sportValue);     
        System.out.println("NOT SPORT VALUE " + notsportValue);     
                
        if (sportValue > notsportValue) {

            System.out.println("LABELED AS " + LABEL1);
        } else {
            System.out.println("LABELED AS " + LABEL2);

        }
    }

}

class Sentence {

    private String kalimat;
    private String TAG;

    public Sentence(String kalimat, String TAG) {
        this.kalimat = kalimat;
        this.TAG = TAG;
    }

    public String getKalimat() {
        return kalimat;
    }

    public String getTAG() {
        return TAG;
    }

}
